from django.db import models
from django.contrib.auth.models import User

class Role(models.Model):
    type = models.CharField(max_length=30, unique=True)


class Permission(models.Model):
    type = models.CharField(max_length=30, unique=True)


class RolesPermission(models.Model):
    role_id = models.ForeignKey(Role, on_delete=models.CASCADE)
    permission_id = models.ForeignKey(
        Permission, on_delete=models.CASCADE, default=None
    )


class UserRole(models.Model):
    role_id = models.ForeignKey(Role, on_delete=models.CASCADE)
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
