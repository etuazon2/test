from rest_framework.decorators import api_view, authentication_classes, permission_classes
from .models import UserRole, RolesPermission, Permission
from django.core import serializers
from django.http import HttpResponse
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated

@api_view(["GET"])
@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
def get_user_permissions(request, user_id):
    roles = UserRole.objects.filter(user_id=request.user.id).values()
    permission_roles = []
    for role in roles:
        permission_roles.append(role["id"])
    permission_list = RolesPermission.objects.filter(role_id__in=permission_roles)
    permission_names = Permission.objects.filter(
        id__in=permission_list.values("permission_id")
    )
    data = serializers.serialize("json", permission_names, fields=("id", "type"))
    return HttpResponse(data, content_type="application/json")
