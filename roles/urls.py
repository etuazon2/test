from django.urls import path
from .views import (
    get_user_permissions,
)

urlpatterns = [
    path("users/<int:user_id>/permissions", get_user_permissions),
]
