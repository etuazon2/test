# Installation
-Install **virtualenv** package `pip install virtualenv` If ubuntu `sudo apt install python3-venv`.
- Run `virtualenv env`. Please make sure outside root directory project. If ubuntu `python3 -m venv env`.
- Run If windows `cd env/Scripts` then type `activate`. If ubuntu `source env/bin/activate`.
- Run `pip install -r requirements.txt`.
- Go to root directory project `cd tests`
- Run `python manage.py makemigrations`
- Run `python manage.py migrate`
- Run `python manage.py runserver`
- Access it using `http://127.0.0.1:8000`

# Postman Collection
- Access postman collection in the directory name `Tests.postman_collection.json`