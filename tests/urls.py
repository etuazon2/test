"""tests URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from rest_framework.authtoken import views
from rest_framework import routers
from roles.viewset import RoleViewset, RolePermissionViewset, PermissionViewset, UserRoleViewset

router = routers.DefaultRouter()
router.register(r'roles', RoleViewset)
router.register(r'permissions', PermissionViewset)
router.register(r'roles/(?P<role_id>\d+)/permissions', RolePermissionViewset)
router.register(r'users/(?P<user_id>\d+)/roles', UserRoleViewset)

urlpatterns = [
    path("admin/", admin.site.urls),
    path("", include("api.urls")),
    path("login", views.obtain_auth_token, name="login"),
    path('', include(router.urls)),
    path("", include("roles.urls")),
]
