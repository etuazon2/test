from django.shortcuts import get_object_or_404
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet
from .models import Role, RolesPermission, Permission, UserRole
from .serializers import RoleSerializer, RolesPermissionSerializer, PermissionSerializer, UserRoleSerializer
from rest_framework import viewsets
from django.core import serializers
from django.http import HttpResponse
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated

class RoleViewset(viewsets.ModelViewSet):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = RoleSerializer
    queryset = Role.objects.all()


class PermissionViewset(viewsets.ModelViewSet):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = PermissionSerializer
    queryset = Permission.objects.all()

class UserRoleViewset(viewsets.ModelViewSet):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = UserRoleSerializer
    queryset = UserRole.objects.all()

    def create(self, request, user_id=None):
        payload = {"role_id": request.data["role_id"], "user_id": user_id}
        serializer = UserRoleSerializer(data=payload)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)

        return Response(serializer.errors)

    def list(self, request, user_id=None):
        queryset = UserRole.objects.filter(user_id=user_id)
        data = serializers.serialize(
            "json", queryset, fields=("role_id", "user_id")
        )
        return HttpResponse(data, content_type="application/json")


class RolePermissionViewset(viewsets.ModelViewSet):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = RolesPermissionSerializer
    queryset = RolesPermission.objects.all()

    def create(self, request, role_id=None):
        payload = {"permission_id": request.data["permission_id"], "role_id": role_id}
        serializer = RolesPermissionSerializer(data=payload)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)

        return Response(serializer.errors)

    def list(self, request, role_id=None):
        queryset = RolesPermission.objects.filter(role_id=role_id)
        data = serializers.serialize(
            "json", queryset, fields=("role_id", "permission_id")
        )
        return HttpResponse(data, content_type="application/json")
