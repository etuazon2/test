from django.test import TestCase
from rest_framework import status
from django.contrib.auth.models import User
from django.urls import reverse

# User = get_user_model()


class AuthenticateTestCase(TestCase):
    def setUp(self):
        sample = User.objects.create_user(
            username="sample",
            password="!Qwe23@#",
            email="sample@sample.com",
        )

    def test_authenticate_can_register(self):
        response = self.client.post(
            reverse("signup"),
            {
                "username": "sample1",
                "password": "!Qwe23@#1",
                "password_confirm": "!Qwe23@#1",
                "email": "sample1@sample.com",
            },
            format="json",
        )
        print("testing registration")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        print("done")
        # assert(len(data) == 1)
        # assert(data == [{'name': 'MegaCorp', 'status': 'active'}])

    def test_authenticate_unable_register(self):
        response = self.client.post(
            reverse("signup"),
            {
                "username": "sample",
                "password": "!Qwe23@#",
                "password_confirm": "!Qwe23@#",
                "email": "sample@sample.com",
            },
            format="json",
        )
        print("testing unable to register")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        print("done")

    def test_authenticate_can_login(self):
        response = self.client.post(
            reverse("login"),
            {"username": "sample", "password": "!Qwe23@#"},
            format="json",
        )
        print("testing login")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        print("done")

    def test_authenticate_unable_login(self):
        response = self.client.post(
            reverse("login"),
            {"username": "sample1", "password": "!Qwe23@#"},
            format="json",
        )
        print("testing unable to login")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        print("done")
