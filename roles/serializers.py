from rest_framework import serializers
from .models import Role, RolesPermission, Permission, UserRole


class RoleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Role
        fields = "__all__"


class PermissionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Permission
        fields = "__all__"


class RolesPermissionSerializer(serializers.ModelSerializer):
    class Meta:
        model = RolesPermission
        fields = "__all__"
    
class UserRoleSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserRole
        fields = "__all__"
